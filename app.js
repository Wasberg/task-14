// 1.
function multiply(a, b) {
    return a * b;
}

// 2.
function add(c, d, e) {
    return c + d + e;
}

// 3.
function divide(f) {
    return f / 2;
}

// 4.
function algebra(g, h, i) {
    return (g + h) * i;
}

// Block scoped variables
function blockScope() { //Block scope is only availible when variables live inside if conditions or in loops. 
    if(true) {          //You can use let and const to declare variables to only exist within the block.
        let itSnow = "Winter";
        const antly = "Summer";
    }
    console.log(itSnow); // cannot be accessed since variable only lives within if condition.
    console.log(antly);
}

// Hoisting
console.log(sail);   // Makes your function availiable even if it is below the command that calls it to act.

function sail() {  
    console.log("You hoisted the sail, gz!")
}

// Coercion
const plus = 1 + 2;   // Adds different types of values to eachother either as numbers or as strings. Strings > numbers.
const concatinate = "hello " + "world"; 
const wierd = "1" + 2;
const wierder = "3" - 1;
const wierderer = true + 5;
const howAbout = "hello " * 5;
const howAbouter = "hello " * "world";

// Employee function
function Employee(id, name, surname, email) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.fullName = function(){
        return this.name + " " + this.surname;
    };
    this.contactCard = function(){
        return {
            empId: this.id,
            name: this.fullName()

        }
    };
}

const alex = new Employee(1337, "Alexander", "The Great", "alexdagreat@greatermail.com");

// Object the Project
const myProject = {
    name: "World Domination",
    deadline: "Before the end times",
    description: "Unite the world under one leader.",
    isActive: true,
    demands: "1 000 000 $",
    involved: {
        leader: "Julius Ceasar",
        general: "Otto von Bismark",
        headOfPress: "Goebbels",
        ministerOfHealth: "Arnold Schwarzenegger",
    }
}